package me.creepinson.mod.util.client.glutils.animation;


import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

/**
 * Made By Creepinson
 */
public class ObjAnimationState {
    public Vector4f rotation = new Vector4f(0, 0, 0, 0);
    public Vector3f translation = new Vector3f(0, 0, 0);
}
